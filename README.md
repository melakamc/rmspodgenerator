# README #

This script automates all the steps required in order to create a new version of RMSFoundation CocoaPod

### What is this repository for? ###

1.  Get the latest tag from RMSFoundation repo and display
2.  User enters a new tag 
3.  The new tag is created on RMSFoundation and pushed to BitBucket
4.  New folder is created under RMSCocoaPods/RMSFoundation
5.  A copy of "RMSFoundation-template.podspec" is put into the newly created folder
6.  Author name, Author email is taken from the user and .podspec file is updated. Tag is taken from previous input

### How do I get set up? ###

EASY! 

Step 1: Just download the script file and place it in your projects folder 

Step 2: Open the script using a text editor and update the Paths for RMSFoundation and RMSCocoaPods repos

### Contribution guidelines ###

* If anyone wants to contribute, please fork the repo and send a pull request.