#!/bin/sh
 
#  What does this script do?
#  1. Get the latest tag from RMSFoundation repo and display
#  2. User enters a new tag 
#  3. The new tag is created on RMSFoundation and pushed to BitBucket
#  4. New folder is created under RMSCocoaPods/RMSFoundation
#  5. A copy of "RMSFoundation-template.podspec" is put into the newly created folder
#  6. Author name, Author email is taken from the user and .podspec file is updated. Tag is taken from previous input
#  7. 

RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

#Don't forget to update these paths
RMSFoundationPath="$PWD/RMSFoundation" #relative path to the script - PWD represents the current folder
RMSCocoaPodsPath="$PWD/RMSCocoaPods" #relative path to the script - PWD represents the current folder

echo "${YELLOW}RMSCocoaPods Path - $RMSCocoaPodsPath"
echo "RMSFoundation Path - $RMSFoundationPath${NC}"

read -p "Before we start, make sure you have committed all the changes you need in RMSFoundation repo (y/n) " choice
case "$choice" in 
  y|Y ) echo "Continuing...";;
  n|N ) echo "${RED}Exiting...${NC}"; exit 0 ;;
  * ) echo "${RED}Exiting...${NC}"; exit 0 ;;
esac

#creating a tag in RMSFoundation project
set latest_tag = 0

git -C "$RMSFoundationPath" pull
latest_tag=$(git -C "$RMSFoundationPath" describe)
if [ "$latest_tag" = "0" ];  then
    echo "${RED}Oops! couldn't find the $RMSFoundationPath repo${NC}"
fi

read -p "Whats the tag name you want to put? (last tag was $latest_tag) " new_tag


read -p "Are you sure you want to create the new tag $new_tag? (y/n) " choice
case "$choice" in 
  y|Y ) echo "Generating..";;
  n|N ) echo "${RED}Exiting...${NC}"; exit 0 ;;
  * ) echo "${RED}Invalid input. Exiting...${NC}"; exit 0 ;;
esac

echo "${YELLOW}Creating new tag ($new_tag)${NC}"
git -C "$RMSFoundationPath" tag -a "$new_tag" -m "$new_tag"

echo "${YELLOW}Pushing new tag to Bitbucket${NC}"
git -C "$RMSFoundationPath" push -v origin refs/tags/"$new_tag"


#creating a podspec in RMSCocoaPods
echo "${YELLOW}Creating new folder under $RMSCocoaPodsPath/Specs/RMSFoundation${NC}"
sudo mkdir "$RMSCocoaPodsPath"/Specs/RMSFoundation/"$new_tag" 
echo "${YELLOW}Copying RMSFoundation-template.podspec into $RMSCocoaPodsPath/Specs/RMSFoundation/$new_tag folder${NC}"
sudo cp "$RMSCocoaPodsPath"/RMSFoundation-template.podspec "$RMSCocoaPodsPath"/Specs/RMSFoundation/$new_tag/RMSFoundation.podspec 
echo "${YELLOW}Renaming file name${NC}"

file_name="$RMSCocoaPodsPath"/Specs/RMSFoundation/$new_tag/RMSFoundation.podspec

echo "Author Name?"
read author_name

echo "Author Email?"
read author_email

echo "${YELLOW}Updating Tag, Author name and author email details${NC}"

sudo sed -ie -e "s/SH_TAG/$new_tag/g" "$file_name"
sudo sed -ie -e "s/SH_AUTHOR_NAME/$author_name/g" "$file_name"
sudo sed -ie -e "s/SH_AUTHOR_EMAIL/$author_email/g" "$file_name"

#Removing the backup file
echo "${YELLOW}Removing if there's any backup files${NC}"
sudo rm "$RMSCocoaPodsPath"/Specs/RMSFoundation/$new_tag/RMSFoundation.podspece

echo "${YELLOW}Committing RMSCocoaPods${NC}"
git -C "$RMSCocoaPodsPath" add .
git -C "$RMSCocoaPodsPath" commit -a -m "$new_tag"
echo "${YELLOW}Pushing RMSCocoaPods${NC}"
git -C "$RMSCocoaPodsPath" push

echo "Done! New Cocoapod version created ~> $new_tag"